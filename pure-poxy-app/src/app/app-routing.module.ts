import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoresComponent } from './views/stores/stores.component';
import { ClientsComponent } from './views/clients/clients.component';
import { ProductsComponent } from './views/products/products.component';
import { HomeComponent } from './views/home/home.component';
import { InventoriesComponent } from './views/inventories/inventories.component';
import { SalesComponent } from './views/sales/sales.component';
import { AccountingComponent } from './views/accounting/accounting.component';

const routes: Routes = [
  {
    path: 'stores',
    component: StoresComponent
  },
  {
    path: 'clients',
    component: ClientsComponent
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'inventories',
    component: InventoriesComponent
  },
  {
    path: 'sales',
    component: SalesComponent
  },
  {
    path: 'accounting',
    component: AccountingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
