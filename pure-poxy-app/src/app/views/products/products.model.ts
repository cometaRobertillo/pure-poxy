export class Product {
    id: number = 0;
    storeId: number = 0;
    storeName: string = '';
    name: string = '';
    description: string = '';
    salePrice: number = 0;
    initialPrice: number = 0;
}