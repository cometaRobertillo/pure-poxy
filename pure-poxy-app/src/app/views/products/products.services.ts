import { HttpClient } from '@angular/common/http';
import { Product } from './products.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class ProductsServices {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/products';
    
    public getProducts = ():Promise<Array<Product>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Product>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getProductsByStore = (storeId:number):Promise<Array<Product>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Product>>(`${this.url}?storeId=${storeId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createProduct = (product:Product):Promise<Array<Product>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Product>>(this.url,product).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateProduct = (product:Product):Promise<Array<Product>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<Product>>(this.url, product).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public deleteProduct = (id:number):Promise<Array<Product>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Product>>(`${this.url}?id=${id}`).subscribe(success => resolve(success), error => reject(error));
        });
    }
}