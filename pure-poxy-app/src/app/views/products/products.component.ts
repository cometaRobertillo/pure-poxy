import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { faPlus, faEdit, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ProductsServices } from './products.services';
import { StoreServices } from '../stores/store.services';
import { Product } from './products.model';
import { Store } from '../stores/store.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public faPlus = faPlus;
  public faEdit = faEdit;
  public faTrash = faTrash;
  public faTimes = faTimes;

  public showModal = false;
  public modalEdit = false;
  public showModalDelete = false;
  public stateAction = 'Nuevo';

  public displayedColumns: string[] = ['id', 'name', 'description', 'storeName', 'salePrice', 'initialPrice', 'action'];
  public dataSource = new MatTableDataSource<Product>(new Array<Product>());

  public product: Product = new Product();
  public productUpdate: Product = new Product();
  public productDelete: Product = new Product();

  public stores: Array<Store> = new Array<Store>();

  public closeModal = (event) => {
    this.showModal = false;
    this.showModalDelete = false;
    this.product = new Product();
    this.productUpdate = new Product();
    this.productDelete = new Product();
  }

  public openModalCreate = (event) => {
    this.modalEdit = false;
    this.showModal = true;
    this.stateAction = 'Nuevo';
  }

  public openModalUpdate = (event, product) => {
    this.modalEdit = true;
    this.showModal = true;
    this.stateAction = 'Actualizar';
    this.productUpdate = product;
  }

  public openModalDelete = (event, product) => {
    this.showModalDelete = true;
    this.productDelete = product;
  }

  constructor(private productServices:ProductsServices, private storeService:StoreServices) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.get();
    this.getStores();
    
    
  }

  public getStores = async() => {
    this.stores = await this.storeService.getStores();
  }

  public get = async() => {
    this.dataSource = new MatTableDataSource<Product>(await this.productServices.getProducts());
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public create = async() => {
    if(this.product.name.trim() == '' || this.product.salePrice == 0 || this.product.initialPrice == 0 || this.product.storeId == 0) return;
    let result = await this.productServices.createProduct(this.product);
    
    await this.get();
    this.closeModal(null);
  }

  public update = async() => {
    if(this.productUpdate.name.trim() == '' || this.productUpdate.salePrice == 0 || this.productUpdate.initialPrice == 0 || this.productUpdate.storeId == 0) return;
    let result = await this.productServices.updateProduct(this.productUpdate);
    await this.get();
    this.closeModal(null);
  }

  public delete = async() => {
    let result = await this.productServices.deleteProduct(this.productDelete.id);
    await this.get();
    this.closeModal(null);
  }
}