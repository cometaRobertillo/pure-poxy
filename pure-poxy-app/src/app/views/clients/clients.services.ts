import { HttpClient } from '@angular/common/http';
import { Client } from './clients.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class ClientServices {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/clients';
    
    public getClients = ():Promise<Array<Client>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Client>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createClient = (client:Client):Promise<Array<Client>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Client>>(this.url,client).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateClient = (client:Client):Promise<Array<Client>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<Client>>(this.url, client).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public deleteClient = (id:number):Promise<Array<Client>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Client>>(`${this.url}?id=${id}`).subscribe(success => resolve(success), error => reject(error));
        });
    }
}