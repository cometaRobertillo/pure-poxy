import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { faPlus, faEdit, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ClientServices } from './clients.services';
import { Client } from './clients.model';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  public faPlus = faPlus;
  public faEdit = faEdit;
  public faTrash = faTrash;
  public faTimes = faTimes;

  public showModal = false;
  public modalEdit = false;
  public showModalDelete = false;
  public stateAction = 'Nuevo';

  public displayedColumns: string[] = ['id', 'name', 'contactName', 'contact', 'action'];
  public dataSource = new MatTableDataSource<Client>(new Array<Client>());

  public client: Client = new Client();
  public clientUpdate: Client = new Client();
  public clientDelete: Client = new Client();

  public closeModal = (event) => {
    this.showModal = false;
    this.showModalDelete = false;
    this.client = new Client();
    this.clientUpdate = new Client();
    this.clientDelete = new Client();
  }

  public openModalCreate = (event) => {
    this.modalEdit = false;
    this.showModal = true;
    this.stateAction = 'Nueva';
  }

  public openModalUpdate = (event, client) => {
    this.modalEdit = true;
    this.showModal = true;
    this.stateAction = 'Actualizar';
    this.clientUpdate = client;
  }

  public openModalDelete = (event, client) => {
    this.showModalDelete = true;
    this.clientDelete = client;
  }

  constructor(private clientServices:ClientServices) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.get();
    
  }

  public get = async() => {
    this.dataSource = new MatTableDataSource<Client>(await this.clientServices.getClients());
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public create = async() => {
    if(this.client.name.trim() == '') return;
    let result = await this.clientServices.createClient(this.client);
    await this.get();
    this.closeModal(null);
  }

  public update = async() => {
    if(this.clientUpdate.name.trim() == '') return;
    let result = await this.clientServices.updateClient(this.clientUpdate);
    await this.get();
    this.closeModal(null);
  }

  public delete = async() => {
    let result = await this.clientServices.deleteClient(this.clientDelete.id);
    await this.get();
    this.closeModal(null);
  }
}