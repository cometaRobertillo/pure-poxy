import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { faPlus, faEdit, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';
import { SalesServices } from './sales.services';
import { ISale, Sale, ProductSale, ExistenceProduct } from './sales.model';

import { StoreServices } from '../stores/store.services';
import { Store } from '../stores/store.model';
import { Client } from '../clients/clients.model';
import { ClientServices } from '../clients/clients.services';


@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  constructor(private saleServices:SalesServices,private storeService:StoreServices, private clientsService: ClientServices) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  ngOnInit() {
    this.get();
    
    this.getStores();
    this.getClients();
  }
  
  public products:Array<ExistenceProduct> = new Array<ExistenceProduct>();
  public stores: Array<Store> = new Array<Store>();
  public productsSelected: Array<number> = [];
  public productsToSell: Array<ProductSale> = new Array<ProductSale>();
  public clients: Array<Client> = new Array<Client>();

  public faPlus = faPlus;
  public faEdit = faEdit;
  public faTrash = faTrash;
  public faTimes = faTimes;

  public showModal = false;
  public showEditModal = false;
  public stateAction = 'Nuevo';

  public displayedColumns: string[] = ['id', 'storeName', 'clientName', 'totalSale', 'date', 'cancelation','action'];
  public dataSource = new MatTableDataSource<ISale>(new Array<ISale>());

  public sale: Sale = new Sale();
  public saleUpdate: Sale = new Sale();

  public showPayment = false;

  public closeModal = (event) => {
    this.showModal = false;
    this.showEditModal = false;
    this.showPayment = false;
    this.sale = new Sale();
    this.saleUpdate = new Sale();
    this.productsSelected = [];
    this.products = new Array<ExistenceProduct>();
    this.productsToSell = new Array<ProductSale>();
  }

  public openModalCreate = (event) => {
    this.showModal = true;
    this.stateAction = 'Nueva';
  }

  public openModalUpdate = async(event, sale) => {
    
    this.showEditModal = true;
    this.stateAction = 'Actualizar';
    this.saleUpdate = sale;
    this.saleUpdate.products = await this.saleServices.getProductSale(sale.id);
    this.saleUpdate.saleDebt = await this.saleServices.getSaleDebt(sale.id);
    if(this.saleUpdate.saleDebt)
      this.saleUpdate.debtPay = this.saleUpdate.saleDebt.debt;
  }
  
  public changeStore = (storeId) => {
    this.getProducts(storeId);
  }
  public getProducts = async(storeId)=> {
    this.products = await this.saleServices.getExistenceProducts(storeId);
  }  
  public getStores = async() => {
    this.stores = await this.storeService.getStores();
  }
  public getClients = async() => {
    this.clients = await this.clientsService.getClients();
  }
  public get = async() => {
    let result = await this.saleServices.getSales(); 
    this.dataSource = new MatTableDataSource<ISale>(result);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public create = async() => {
    if(!this.sale.clientId)return;
    
    let result = await this.saleServices.createSale(this.sale);
    
    await Promise.all(
      this.productsToSell.map((product:ProductSale) => {
        product.saleId = result.id;
        this.saleServices.createProductSale(product);
      })
    );
    await this.get();
    this.closeModal(null);
  }

  public update = async() => {
    if(this.saleUpdate.debtPay == 0)return;
    let result = await this.saleServices.updateSale(this.saleUpdate);
    this.get();
    this.closeModal(null);
  }

  public cancelate = async() => {
    let result = await this.saleServices.cancelateSale(this.saleUpdate.id);
    this.get();
    this.closeModal(null);
  }

  public checkSelected = (product:ExistenceProduct) => {

    let index = this.productsSelected.indexOf(product.id);

    if(index > -1){

      this.productsSelected.splice(index, 1);
      this.productsToSell.splice(index, 1);
    }else{

      this.productsToSell.push({
        id: 0,
        productId: product.id,
        name: product.productName,
        saleId: 0,
        quantity: 1,
        salePrice: product.salePrice,
        maxQuantity: product.quantity
      });
      this.productsSelected.push(product.id);
    }

    this.calculateTotal();
  }

  public changeQuantity = (product:ProductSale) => {
    if(product.quantity > product.maxQuantity)
      product.quantity = product.maxQuantity;

    this.calculateTotal();
  }

  public calculateTotal = () => {
    if(this.productsToSell.length > 0) {
      let total = 0;
      this.productsToSell.map((product:ProductSale)=> {
        total = total + (product.salePrice * product.quantity);
      });
      this.sale.totalSale = total;
    }else {
      this.sale.totalSale = 0;
    }
  }

  public nextStep = () => {
    this.showPayment = true;
  }

  public backStep = () => {
    this.showPayment = false;
  }

  public debtPayChange = (debtPay) => {
    if(debtPay > this.saleUpdate.saleDebt.debt){
      this.saleUpdate.saleDebt.debt
    }
  }
}