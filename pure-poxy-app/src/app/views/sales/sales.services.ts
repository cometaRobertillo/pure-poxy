import { HttpClient } from '@angular/common/http';
import { Sale, ExistenceProduct, ProductSale, SaleDebt } from './sales.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class SalesServices {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/sales';
    
    public getExistenceProducts =(storeId):Promise<Array<ExistenceProduct>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<ExistenceProduct>>(`${this.url}/existence?storeId=${storeId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getSales = ():Promise<Array<Sale>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Sale>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getSalesByStore = (storeId:number):Promise<Array<Sale>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Sale>>(`${this.url}/store?storeId=${storeId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createSale = (sale:Sale):Promise<Sale> => {
        return new Promise((resolve,reject) => {
            this.http.post<Sale>(this.url,sale).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateSale = (sale:Sale):Promise<Array<Sale>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<Sale>>(this.url, sale).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public cancelateSale = (id:number):Promise<Array<Sale>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Sale>>(`${this.url}?id=${id}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getProductSale = (saleId:number):Promise<Array<ProductSale>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<ProductSale>>(`${this.url}/productsales?saleId=${saleId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createProductSale = (product:ProductSale):Promise<ProductSale> => {
        return new Promise((resolve,reject) => {
            this.http.post<ProductSale>(`${this.url}/productsales`,product).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getSaleDebt = (saleId:number):Promise<SaleDebt> => {
        return new Promise((resolve,reject) => {
            this.http.get<SaleDebt>(`${this.url}/debt?saleId=${saleId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }
}