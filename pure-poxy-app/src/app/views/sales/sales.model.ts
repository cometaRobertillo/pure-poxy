export class SaleDebt {
    id: number = 0;
    saleId: number = 0;
    storeName: string = '';
    clientId: number = 0;
    clientName: string = '';
    debt: number = 0;
}

export class ExistenceProduct {
    id: number = 0;
    inventoryId: number = 0;
    productId: number = 0;
    productName: string = '';
    quantity: number = 0;
    salePrice: number = 0;
}

export class ProductSale {
    id: number = 0;
    saleId: number = 0;
    productId: number = 0;
    name: string = '';
    quantity: number = 0;
    salePrice: number = 0;
    maxQuantity: number = 0;
}

export interface ISale {
    id: number;
    storeId: number;
    storeName: string;
    clientId: number;
    clientName: string;
    totalSale: number;
    clientPayment: number;
    cancelation: boolean;
  }

export class Sale {
    id: number = 0;
    storeId: number = 0;
    storeName: string = '';
    clientId: number = 0;
    clientName: string = '';
    totalSale: number = 0;
    clientPayment: number = 0;
    cancelation: boolean = false;
    products: Array<ProductSale> = new Array<ProductSale>();
    saleDebt: SaleDebt = new SaleDebt();
    debtPay: number = 0;
}