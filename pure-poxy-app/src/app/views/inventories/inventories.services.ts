import { HttpClient } from '@angular/common/http';
import { Inventory, Existence } from './inventories.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class InventoriesServices {
    constructor(private http: HttpClient){}

    private urlInventory = 'http://localhost:3000/api/inventories';
    private urlExistence = 'http://localhost:3000/api/existences';
    
    public getInventories = ():Promise<Array<Inventory>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Inventory>>(this.urlInventory).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getExistence = (inventoryId: number):Promise<Array<Existence>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Existence>>(`${this.urlExistence}?inventoryId=${inventoryId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createExistence = (existence:Existence):Promise<Array<Existence>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Existence>>(this.urlExistence,existence).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateExistence = (existence:Existence):Promise<Array<Existence>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<Existence>>(this.urlExistence, existence).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public deleteExistence = (id:number, inventoryId: number):Promise<Array<Existence>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Existence>>(`${this.urlExistence}?id=${id}&inventoryId=${inventoryId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public makeTransact = (traspass: {fromInventoryId: number, productId: number, existenceId:number, quantity:number, toInventoryId:number} ):Promise<Array<Existence>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Existence>>(`${this.urlExistence}/transfer`, traspass).subscribe(success => resolve(success), error => reject(error));
        });
    }
}