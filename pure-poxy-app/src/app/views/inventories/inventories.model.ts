export class Inventory {
    id: number = 0;
    storeId: number = 0;
    storeName: string = '';
}

export class Existence {
    id: number = 0;
    inventoryId: number = 0;
    productId: number = 0;
    productName: string = '';
    quantity: number = 0;
}