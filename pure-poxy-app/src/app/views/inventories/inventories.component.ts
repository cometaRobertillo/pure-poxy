import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { faPlus, faEdit, faDolly, faTimes, faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { InventoriesServices } from './inventories.services';
import { Inventory, Existence } from './inventories.model';
import { ProductsServices } from '../products/products.services';
import { Product } from '../products/products.model';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.scss']
})
export class InventoriesComponent implements OnInit {

  constructor(private inventoryService:InventoriesServices, private productsService: ProductsServices) { }

  public faPlus = faPlus;
  public faEdit = faEdit;
  public faTimes = faTimes;
  public faAngleLeft = faAngleLeft;
  public faDolly = faDolly;
  
  public detail = false
  public showModal = false;
  public modalEdit = false;
  public transfer = false;
  public stateAction = 'Agregar';

  public inventory: Inventory = new Inventory();
  public existence: Existence = new Existence();
  public existenceUpdate: Existence = new Existence();

  public inventories: Array<Inventory> = new Array<Inventory>();
  public existences: Array<Existence> = new Array<Existence>();
  public products: Array<Product> = new Array<Product>();

  public toInventory: Inventory = new Inventory();

  public traspass: {fromInventoryId: number, productId: number, existenceId:number, quantity:number, toInventoryId:number} = {
    fromInventoryId: 0,
    productId: 0,
    existenceId: 0,
    quantity: 0,
    toInventoryId: 0
  };

  public back = (event) => {
    this.inventory = new Inventory();
    this.existence = new Existence();
    this.existences = [];
    this.products = [];

    this.detail = false;
  }

  public showDetail = async(event, inventory) => {
    this.inventory = inventory;

    await this.get();
    await this.getProducts();
    this.detail = true;
  }

  public closeModal = (event) => {
    this.showModal = false;
    this.transfer = false;
    this.existence = new Existence();
    this.existenceUpdate = new Existence();
  }

  public openModalCreate = (event) => {
    this.modalEdit = false;
    this.transfer = false;
    this.showModal = true;
    this.stateAction = 'Agregar producto al inventario';
  }
  
  public openModalTransfer = (event) => {
    this.modalEdit = false;
    this.transfer = true;
    this.showModal = true;
    this.traspass.fromInventoryId = this.inventory.id;
    this.stateAction = 'Traspaso de inventarios';
  }

  public openModalUpdate = (event, existence) => {
    this.modalEdit = true;
    this.transfer = false;
    this.showModal = true;
    this.stateAction = 'Actualizar producto del inventario';
    this.existenceUpdate = existence;
  }


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.getInventories();
  }

  public getInventories = async() => {
    this.inventories = await this.inventoryService.getInventories();
  }

  public get = async() => {
    this.existences = await this.inventoryService.getExistence(this.inventory.id);
  }
  public getProducts = async()=> {
    this.products = await this.productsService.getProductsByStore(this.inventory.storeId);
    this.products.map((product, index)=> {
      let filter = this.existences.filter(x => x.productId == product.id);
      if(filter.length > 0){
        this.products.splice(index, 1);
      }
    });
  }
  public create = async() => {
    this.existence.inventoryId = this.inventory.id;
    let result = await this.inventoryService.createExistence(this.existence);
    
    this.existences = result;
    this.closeModal(null);
  }

  public update = async() => {
    
    let result = await this.inventoryService.updateExistence(this.existenceUpdate);
    this.existences = result;
    this.closeModal(null);
  }

  public delete = async() => {
    let result = await this.inventoryService.deleteExistence(this.existenceUpdate.id, this.existenceUpdate.inventoryId);
    this.existences = result;
    this.closeModal(null);
  }

  public selectChangeTo = (event,toInventoryId: number) => {
    let inventory = this.inventories.filter(a => a.id == toInventoryId);
    this.toInventory = inventory[0];
  }

  public selectExistenceChange = (event, productId: number) => {
    let existence = this.existences.filter(x => x.productId == productId);
    if(existence.length > 0){
      this.traspass.existenceId = existence[0].id;
    }
  }

  public checkQuantity = (quantity: number) => {
    
    if(quantity < 0) {
      this.traspass.quantity = 0;
      return;
    }
    
    let result = this.existences.filter(x => x.productId == this.traspass.productId);
    
    if(result.length > 0){
      if(quantity > result[0].quantity)
        this.traspass.quantity = result[0].quantity;
        return;
    }else{
      return;
    }
  }

  public createTransact = async() => {
    if(this.traspass.quantity == 0 || this.traspass.productId == 0 || this.traspass.toInventoryId == 0 ) return;

    let check = this.existences.filter(x => x.productId == this.traspass.productId);
    
    if(check.length > 0){
      if(this.traspass.quantity > check[0].quantity)
        this.traspass.quantity = check[0].quantity;
    }else{
      return;
    }

    let result = await this.inventoryService.makeTransact(this.traspass);
    this.existences = result;
    this.closeModal(null);
  }
}