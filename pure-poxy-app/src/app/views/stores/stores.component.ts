import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { faPlus, faEdit, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';
import {StoreServices} from './store.services';
import { Store } from './store.model';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {

  public faPlus = faPlus;
  public faEdit = faEdit;
  public faTrash = faTrash;
  public faTimes = faTimes;

  public showModal = false;
  public modalEdit = false;
  public showModalDelete = false;
  public stateAction = 'Nueva';

  public displayedColumns: string[] = ['id', 'name', 'action'];
  public dataSource = new MatTableDataSource<Store>(new Array<Store>());

  public store: Store = new Store();
  public storeUpdate: Store = new Store();
  public storeDelete: Store = new Store();

  public closeModal = (event) => {
    this.showModal = false;
    this.showModalDelete = false;
    this.store = new Store();
    this.storeUpdate = new Store();
    this.storeDelete = new Store();
  }

  public openModalCreate = (event) => {
    this.modalEdit = false;
    this.showModal = true;
    this.stateAction = 'Nueva';
  }

  public openModalUpdate = (event, store) => {
    this.modalEdit = true;
    this.showModal = true;
    this.stateAction = 'Actualizar';
    this.storeUpdate = store;
  }

  public openModalDelete = (event, store) => {
    this.showModalDelete = true;
    this.storeDelete = store;
  }

  constructor(private storeServices:StoreServices) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.get();
    
  }

  public get = async() => {
    this.dataSource = new MatTableDataSource<Store>(await this.storeServices.getStores());
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public create = async() => {
    if(this.store.name.trim() == '') return;
    let result = await this.storeServices.createStore(this.store);
    await this.get();
    this.closeModal(null);
  }

  public update = async() => {
    if(this.storeUpdate.name.trim() == '') return;
    let result = await this.storeServices.updateStore(this.storeUpdate);
    await this.get();
    this.closeModal(null);
  }

  public delete = async() => {
    let result = await this.storeServices.deleteStore(this.storeDelete.id);
    await this.get();
    this.closeModal(null);
  }
}