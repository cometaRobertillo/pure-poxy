import { HttpClient } from '@angular/common/http';
import { Store } from './store.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class StoreServices {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/stores';
    
    public getStores = ():Promise<Array<Store>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Store>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createStore = (store:Store):Promise<Array<Store>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Store>>(this.url,store).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateStore = (store:Store):Promise<Array<Store>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<Store>>(this.url, store).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public deleteStore = (id:number):Promise<Array<Store>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Store>>(`${this.url}?id=${id}`).subscribe(success => resolve(success), error => reject(error));
        });
    }
}