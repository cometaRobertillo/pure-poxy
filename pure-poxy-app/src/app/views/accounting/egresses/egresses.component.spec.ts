import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EgressesComponent } from './egresses.component';

describe('EgressesComponent', () => {
  let component: EgressesComponent;
  let fixture: ComponentFixture<EgressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EgressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EgressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
