import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account, Outcome } from '../accounting.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-egresses',
  templateUrl: './egresses.component.html',
  styleUrls: ['./egresses.component.scss']
})
export class EgressesComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
    this.get();
  }
  ngOnDestroy() {
    this.account = new Account();
    this.dataSource = new MatTableDataSource<Outcome>(new Array<Outcome>());
  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();

  public faTrash = faTrash;
  public faTimes = faTimes;
  public displayedColumns: string[] = ['id', 'description', 'typeName', 'cost', 'action'];
  public dataSource = new MatTableDataSource<Outcome>(new Array<Outcome>());

  public account: Account = new Account();

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public get = async() => {
    this.dataSource = new MatTableDataSource<Outcome>(await this.accountServices.getOutcomesByAccount(this.account.id));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public delete = async(event,outcome:Outcome)=> {
    let result = await this.accountServices.deleteOutcome(outcome.id, this.account.id);
    await this.get();
  }
}
