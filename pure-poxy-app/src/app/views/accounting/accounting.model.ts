export class Outcome {
    id: number = 0;
    accountId: number = 0;
    typeId: number = 0;
    typeName: string = '';
    description: string = '';
    cost: number = 0;
}

export class ToPay {
    id: number = 0;
    accountId: number = 0;
    description: string = '';
    debt: number = 0;
}

export interface OutcomeType {
    id: number;
    name: string;
}

export class Account {
    id: number = 0;
    storeId: number = 0;
    storeName: string = '';
    current: number = 0;
}