import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account } from '../accounting.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { faTrash, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tocharge',
  templateUrl: './tocharge.component.html',
  styleUrls: ['./tocharge.component.scss']
})
export class TochargeComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
    this.get();
  }
  ngOnDestroy() {
    this.account = new Account();
    this.dataSource = new MatTableDataSource<any>(new Array<any>());
  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();

  public faTrash = faTrash;
  public faTimes = faTimes;
  public displayedColumns: string[] = ['clientId', 'clientName', 'debt'];
  public dataSource = new MatTableDataSource<any>(new Array<any>());

  public account: Account = new Account();

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public get = async() => {
    let result = await this.accountServices.toCharge(this.account.storeId);
    console.log(result)
    this.dataSource = new MatTableDataSource<any>(result);

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
