import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TochargeComponent } from './tocharge.component';

describe('TochargeComponent', () => {
  let component: TochargeComponent;
  let fixture: ComponentFixture<TochargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TochargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TochargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
