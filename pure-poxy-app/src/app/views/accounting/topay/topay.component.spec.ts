import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopayComponent } from './topay.component';

describe('TopayComponent', () => {
  let component: TopayComponent;
  let fixture: ComponentFixture<TopayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
