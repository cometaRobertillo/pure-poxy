import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account, ToPay } from '../accounting.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { faTrash, faTimes, faPlus, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-topay',
  templateUrl: './topay.component.html',
  styleUrls: ['./topay.component.scss']
})
export class TopayComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
    this.get();
  }
  ngOnDestroy() {
    this.account = new Account();
    this.dataSource = new MatTableDataSource<ToPay>(new Array<ToPay>());
  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();

  public faTrash = faTrash;
  public faTimes = faTimes;
  public faPlus = faPlus;
  public faEdit = faEdit;
  public creating = false;
  public editing = false;

  public displayedColumns: string[] = ['id', 'description', 'debt', 'action'];
  public dataSource = new MatTableDataSource<ToPay>(new Array<ToPay>());
  
  public topay: ToPay = new ToPay();
  public topayEdit: ToPay = new ToPay();
  public account: Account = new Account();

  public closeModal = (event) => {
    this.close.emit(event);
  }
  public closeView = (event) => {
    this.creating = false;
    this.editing = false;
    this.topay = new ToPay();
    this.topayEdit = new ToPay();

  }
  public openCreate = (event) => {
    this.topay = new ToPay();
    this.creating = true;
  }
  public openUpdate = (event, element: ToPay) => {
    this.topayEdit = new ToPay();
    this.topayEdit = element;
    this.editing = true;
  }
  public costChange = (debt: number) => {
    if(debt < 0) debt = 0;
  }
  public get = async() => {
    this.dataSource = new MatTableDataSource<ToPay>(await this.accountServices.getToPay(this.account.id));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  public create = async(event) => {
    if(this.topay.debt <= 0 || this.topay.description.trim().length == 0) return;
    this.topay.accountId = this.account.id;
    let result = await this.accountServices.createToPay(this.topay);
    await this.get();
    this.closeView(null);
  }
  
  public update = async(event) => {
    if(this.topayEdit.debt <= 0 || this.topayEdit.description.trim().length == 0) return;
    console.log(this.topayEdit)
    let result = await this.accountServices.updateToPay(this.topayEdit);
    await this.get();
    this.closeView(null);
  }

  public delete = async(event,topay:ToPay)=> {
    let result = await this.accountServices.deleteToPay(topay.id);
    await this.get();
    
  }

}
