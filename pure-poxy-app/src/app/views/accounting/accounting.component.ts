import { Component, OnInit } from '@angular/core';
import {AccountServices} from './accounting.service';
import { Account } from './accounting.model';
@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.scss']
})
export class AccountingComponent implements OnInit {

  constructor(private accountServices: AccountServices) { }

  ngOnInit() {
    this.get();
  }

  public accounts: Array<Account> = new Array<Account>();
  public accountSelected: Account = new Account();

  public showAddEgress: boolean = false;
  public showEgresses: boolean = false;
  public showWithdraw: boolean = false;
  public showTraspass: boolean = false;
  public showToCharge: boolean = false;
  public showToPay: boolean = false;
  public showAddCurrent: boolean = false;
  
  public closeModal = (event) => {
    this.showAddEgress = false;
    this.showEgresses = false;
    this.showWithdraw = false;
    this.showTraspass = false;
    this.showToCharge = false;
    this.showToPay = false;
    this.showAddCurrent = false;
    this.accountSelected = new Account();
    this.get();
  }

  public openView = (event, modal, account) => {
    if(this[modal]) return;
    
    this.closeModal(event);
    
    this.accountSelected = account;
    this[modal] = true;
  }

  public get = async() => {
    this.accounts = await this.accountServices.getAccounts();
  }
}
