import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCurrentComponent } from './add-current.component';

describe('AddCurrentComponent', () => {
  let component: AddCurrentComponent;
  let fixture: ComponentFixture<AddCurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
