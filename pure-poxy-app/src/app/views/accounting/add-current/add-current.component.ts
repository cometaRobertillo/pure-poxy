import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account } from '../accounting.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-current',
  templateUrl: './add-current.component.html',
  styleUrls: ['./add-current.component.scss']
})
export class AddCurrentComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
  }
  ngOnDestroy() {
    this.account = new Account();
  }

  public faTimes = faTimes;
  public newCurrent: number = 0;

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();
  
  public account: Account = new Account();

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public create = async() => {
    if(this.newCurrent < 0) {
      this.newCurrent = 0;
      return;
    }
    let result = await this.accountServices.addCurrent(this.account.id, this.newCurrent);
    this.closeModal(null);
  }

  public costChange = (current: number) => {
    if(current < 0) this.newCurrent = 0;
  }
}
