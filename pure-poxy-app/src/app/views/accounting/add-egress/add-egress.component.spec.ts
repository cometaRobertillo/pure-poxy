import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEgressComponent } from './add-egress.component';

describe('AddEgressComponent', () => {
  let component: AddEgressComponent;
  let fixture: ComponentFixture<AddEgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
