import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account, Outcome, OutcomeType } from '../accounting.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-egress',
  templateUrl: './add-egress.component.html',
  styleUrls: ['./add-egress.component.scss']
})
export class AddEgressComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
    this.outcome.accountId = this.account.id;
    this.outcome.typeId = 4;
    this.outcome.typeName = 'Otros';
  }
  ngOnDestroy() {
    this.account = new Account();
  }

  public faTimes = faTimes;
  public outcomesTypes: Array<OutcomeType> = [
    {
      id: 1,
      name: 'Pago de fletes'
    },
    {
      id: 2,
      name: 'Compra de productos'
    },
    {
      id: 3,
      name: 'Compra de insumos'
    },
    {
      id: 4,
      name: 'Otros'
    }
  ];
  

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();
  
  public account: Account = new Account();
  public outcome: Outcome = new Outcome();

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public selectChange = (typeId: number) => {
    let type = this.outcomesTypes.filter((o:OutcomeType) => o.id == typeId);
    this.outcome.typeName = type[0].name;
  }

  public costChange = (cost: number) => {
    if(cost < 0) this.outcome.cost = 0;
    if(cost > this.account.current) this.outcome.cost = this.account.current;
  }

  public create = async() => {
    if(this.outcome.cost == 0) return;
    let result = await this.accountServices.createOutcome(this.outcome);
    this.closeModal(null);
  }

}
