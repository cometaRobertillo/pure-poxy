import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account } from '../accounting.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.account = this._account;
    this.withdraw.accountId = this.account.id;
  }
  ngOnDestroy() {
    this.account = new Account();
  }

  @Input() public _account: Account;
  @Output() public close = new EventEmitter();
  public faTimes = faTimes;
  public account: Account = new Account();
  public withdraw: {accountId: number, current: number} = {
    accountId: 0,
    current: 0
  };

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public costChange = (current: number) => {
    if(current < 0) this.withdraw.current = 0;
    if(current > this.account.current) this.withdraw.current = this.account.current;
  }

  public create = async() => {
    if(this.withdraw.current == 0) return;
    let result = await this.accountServices.withdrawCurrent(this.withdraw);
    this.closeModal(null);
  }

}
