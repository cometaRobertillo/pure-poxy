import { HttpClient } from '@angular/common/http';
import { Account, Outcome, ToPay } from './accounting.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class AccountServices {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/accounts';
    
    public getAccounts = ():Promise<Array<Account>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Account>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getOutcomesByAccount = (accountId:number):Promise<Array<Outcome>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<Outcome>>(`${this.url}/outcomes?accountId=${accountId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createOutcome = (outcome:Outcome):Promise<any> => {
        return new Promise((resolve,reject) => {
            this.http.post<any>(`${this.url}/outcomes`, outcome).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public deleteOutcome = (outcomeId:number, accountId:number):Promise<Array<Account>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<Account>>(`${this.url}/outcomes?id=${outcomeId}&accountId=${accountId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public withdrawCurrent = (account:{accountId: number, current: number}):Promise<Account> => {
        return new Promise((resolve,reject) => {
            this.http.post<Account>(`${this.url}/withdraw`, account).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public traspassCurrent = (accounts:{fromAccountId: number, current: number, toAccountId: number}):Promise<Array<Account>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<Account>>(`${this.url}/traspass`, accounts).subscribe(success => resolve(success), error => reject(error));
        });
    }
    
    public toCharge = (storeId: number):Promise<Array<Account>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<any>>(`${this.url}/tocharge?storeId=${storeId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getToPay = (accountId: number):Promise<Array<any>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<any>>(`${this.url}/topay?accountId=${accountId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public createToPay = (topay: ToPay):Promise<Array<ToPay>> => {
        return new Promise((resolve,reject) => {
            this.http.post<Array<ToPay>>(`${this.url}/topay`, topay).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public updateToPay = (topay: ToPay):Promise<Array<ToPay>> => {
        return new Promise((resolve,reject) => {
            this.http.put<Array<ToPay>>(`${this.url}/topay`, topay).subscribe(success => resolve(success), error => reject(error));
        });
    }
    public deleteToPay = (topayId: number):Promise<Array<ToPay>> => {
        return new Promise((resolve,reject) => {
            this.http.delete<Array<ToPay>>(`${this.url}/topay?topayId=${topayId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public addCurrent = (accountId: number, accountNewCurrent: number):Promise<{msg: string, error: any}> => {
        return new Promise((resolve,reject) => {
            this.http.post<{msg: string, error: any}>(`${this.url}/addCurrenct`, {id: accountId, current: accountNewCurrent}).subscribe(success => resolve(success), error => reject(error));
        });
    }
}