import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraspassComponent } from './traspass.component';

describe('TraspassComponent', () => {
  let component: TraspassComponent;
  let fixture: ComponentFixture<TraspassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraspassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraspassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
