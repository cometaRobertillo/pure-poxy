import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { AccountServices } from '../accounting.service';
import { Account } from '../accounting.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-traspass',
  templateUrl: './traspass.component.html',
  styleUrls: ['./traspass.component.scss']
})
export class TraspassComponent implements OnInit, OnDestroy {

  constructor(private accountServices:AccountServices) { }

  ngOnInit() {
    this.accounts = this._accounts;
  }
  ngOnDestroy() {
    this.accounts = new Array<Account>();
  }

  @Input() public _accounts: Array<Account>;
  @Output() public close = new EventEmitter();

  public faTimes = faTimes;
  public accounts: Array<Account> = new Array<Account>();
  public traspass: {fromAccountId: number, current:number, toAccountId:number} = {
    fromAccountId: 0,
    current: 0,
    toAccountId: 0
  };

  public fromAccount: Account = new Account();
  public toAccount: Account = new Account();

  public closeModal = (event) => {
    this.close.emit(event);
  }

  public selectChangeFrom = (event,fromAccountId: number) => {
    let account = this.accounts.filter(a => a.id == fromAccountId);
    this.fromAccount = account[0];
    console.log(this.fromAccount)
  }

  public selectChangeTo = (event,toAccountId: number) => {
    let account = this.accounts.filter(a => a.id == toAccountId);
    this.toAccount = account[0];
    console.log(this.toAccount)
  }

  public costChange = (current: number) => {
    if(current < 0) this.traspass.current = 0;
    if(this.fromAccount.current == 0) this.traspass.current = 0;
    if(current > this.fromAccount.current) this.traspass.current = this.fromAccount.current;
  }

  public create = async() => {
    if(this.traspass.current == 0 || this.traspass.current > this.fromAccount.current) return;
    let result = await this.accountServices.traspassCurrent(this.traspass);
    this.closeModal(null);
  }

}
