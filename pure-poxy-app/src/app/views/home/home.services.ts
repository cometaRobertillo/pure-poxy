import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class HomeService {
    constructor(private http: HttpClient){}

    private url = 'http://localhost:3000/api/home';
    
    public getHome = ():Promise<Array<any>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<any>>(this.url).subscribe(success => resolve(success), error => reject(error));
        });
    }

    public getHomeDetail = (storeId: number):Promise<Array<any>> => {
        return new Promise((resolve,reject) => {
            this.http.get<Array<any>>(`${this.url}detail?storeId=${storeId}`).subscribe(success => resolve(success), error => reject(error));
        });
    }
}