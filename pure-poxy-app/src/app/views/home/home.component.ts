import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.get();
  }

  public stores: Array<any> = [];

  public get = async() => {
    this.stores = await this.homeService.getHome();
    this.stores.map(async(store) => {
      store.detail = await this.homeService.getHomeDetail(store.id);
    });
    console.log(this.stores)
  }
}
