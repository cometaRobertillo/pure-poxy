const Product = require('./products.model');
const Store = require('../stores/stores.model');

const getProducts = async(req,res) => {
    try {
        let products;
        if(req.query.storeId){
            products = await Product.find({storeId: req.query.storeId});
        }else{
            products = await Product.find();
        }
        res.send(products);
    }catch(err){
        res.send({error:err});
    }
}

const createProduct = async(req,res) => {
    try {
        let store = await Store.findOne({id: req.body.storeId});
        await Product.create({
            name: req.body.name,
            description: req.body.description,
            salePrice: req.body.salePrice,
            initialPrice: req.body.initialPrice,
            storeId: req.body.storeId,
            storeName: store.name
        });
        let products = await Product.find();
        res.send(products);
    }catch(err){
        res.send({error:err});
    }
}

const updateProduct = async(req,res) => {
    try {
        let store = await Store.findOne({id: req.body.storeId});

        await Product.updateOne({id: req.body.id}, {
            $set: {
                    name: req.body.name,
                    description: req.body.description,
                    salePrice: req.body.salePrice,
                    initialPrice: req.body.initialPrice,
                    storeId: req.body.storeId,
                    storeName: store.name
                  }
            }
        );

        let products = await Product.find();
        res.send(products);
    }catch(err){
        res.send({error:err});
    }
}

const deleteProduct = async(req,res) => {
    try {
        await Product.deleteOne({id: req.query.id});
        let products = await Product.find();
        res.send(products);
    }catch(err){
        res.send({error:err});
    }
}

const updateProductInternal = async(storeId, storeName) => {
    try {
        await Product.updateMany({storeId: storeId}, {$set: {storeName: storeName}});
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const deleteProductInternal = async(storeId) => {
    try {
        await Product.deleteMany({storeId: storeId});
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}
module.exports = {getProducts, createProduct, updateProduct, deleteProduct, updateProductInternal, deleteProductInternal};