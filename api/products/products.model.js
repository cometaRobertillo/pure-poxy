const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    name: { 
        type: mongoose.SchemaTypes.String,
        required: true
    },
    description: {
        type: mongoose.SchemaTypes.String,
        required: false
    },
    salePrice: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    initialPrice: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
};

const collectionName = "product";
const productSchema = mongoose.Schema(schema);

productSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'product', field: 'id', startAt: 1, incrementBy: 1});

const Product = mongoose.model(collectionName, productSchema);
module.exports = Product;