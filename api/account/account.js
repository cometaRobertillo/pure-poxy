const Account = require('./accounts.model');

const createAccountInternal = async(storeId,storeName) => {
    try {

        await Account.create({
            storeId: storeId,
            storeName: storeName,
            current: 0
        });
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const updateAccountStoreInternal = async(storeInfo) => {
    try {
        await Account.updateOne({storeId: storeInfo.storeId}, { $set: { storeName: storeInfo.storeName } });
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const updateIncrementAccountCurrentInternal = async(storeId, current) => {
    try {
        let account = await Account.findOne({storeId: storeId});
        await Account.updateOne({id: account.id}, { $set: { current: (account.current + current) } });
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const updateDecrementAccountCurrentInternal = async(storeId, current) => {
    try {
        let account = await Account.findOne({storeId: storeId});
        
        await Account.updateOne({id: account.id}, { $set: { current: (account.current - current) } });
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const deleteAccountInternal = async(storeId) => {
    try {

        await Account.deleteOne({storeId: storeId});
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err);
        return {msg: 'error', error: err};
    }
}

const getAccounts = async(req,res) => {
    try {
        let accounts = await Account.find();
        res.send(accounts);
    }catch(err){
        res.send({error:err});
    }
}

const addAccountCurrent = async(req,res) => {
    try{
        let account = await Account.findOne({id: req.body.id});
        await Account.updateOne({id: account.id}, { $set: { current: (account.current + req.body.current) } });
        res.send({msg: 'ok', error: null});
    }catch(err){
        console.log(err);
        res.send({msg: 'error', error: err});
    }
}

module.exports = {addAccountCurrent, createAccountInternal, updateIncrementAccountCurrentInternal, updateDecrementAccountCurrentInternal, updateAccountStoreInternal, deleteAccountInternal, getAccounts};