const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    current: {
        type: mongoose.SchemaTypes.Number,
        default: 0,
        required: true
    }
};

const collectionName = "account";
const accountSchema = mongoose.Schema(schema);

accountSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'account', field: 'id', startAt: 1, incrementBy: 1});

const Account = mongoose.model(collectionName, accountSchema);
module.exports = Account;