const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    saleId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    clientId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    clientName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    debt: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
};


const collectionName = "saleDebt";
const saleDebtSchema = mongoose.Schema(schema);

saleDebtSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'saleDebtSchema', field: 'id', startAt: 1, incrementBy: 1});

const SaleDebt = mongoose.model(collectionName, saleDebtSchema);
module.exports = SaleDebt;