const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    saleId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    productId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    name: { 
        type: mongoose.SchemaTypes.String,
        required: true
    },
    quantity: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    salePrice: {
        type: mongoose.SchemaTypes.Number,
        required: true
    }
};


const collectionName = "productSale";
const productSaleSchema = mongoose.Schema(schema);

productSaleSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'productSaleSchema', field: 'id', startAt: 1, incrementBy: 1});

const ProductSale = mongoose.model(collectionName, productSaleSchema);
module.exports = ProductSale;