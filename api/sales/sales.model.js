const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    clientId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    clientName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    totalSale: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    clientPayment: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    date: {
        type: mongoose.SchemaTypes.Date,
        default: Date.now,
        required: true
    },
    cancelation: {
        type: mongoose.SchemaTypes.Boolean,
        default: false,
        required: false
    }
};


const collectionName = "sale";
const saleSchema = mongoose.Schema(schema);

saleSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'sale', field: 'id', startAt: 1, incrementBy: 1});

const Sale = mongoose.model(collectionName, saleSchema);
module.exports = Sale;