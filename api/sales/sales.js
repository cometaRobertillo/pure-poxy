const Sale = require('./sales.model');
const ProductSale = require('./productSale.model');
const Inventory = require('../inventories/inventories.model');
const Existence = require('../inventories/existences.model');
const Product = require('../products/products.model');
const Client = require('../clients/clients.model');
const SaleDebt = require('./saleDebt.model');
const Store = require('../stores/stores.model');

const {updateIncrementAccountCurrentInternal, updateDecrementAccountCurrentInternal} = require('../account/account');
const {updateDecrementExistenceInventoryInternal, updateIncrementExistenceInventoryInternal} = require('../inventories/inventories');

const getExistenceProducts = async(req,res) => {
    try{
        let inventory = await Inventory.findOne({storeId: req.query.storeId});
        let existences = await Existence.find({inventoryId: inventory.id});
        let existenceProducts = [];
        if(existences.length > 0) {
            await Promise.all(
                existences.map(async(existence) => {
                    let product = await Product.findOne({id: existence.productId});
                    existenceProducts.push({
                        id: existence.id,
                        inventoryId: inventory.id,
                        productId: product.id,
                        productName: product.name,
                        quantity: existence.quantity,
                        salePrice: product.salePrice
                    })
                })
            )
        }
        res.send(existenceProducts);
    }catch(err){
        res.send({error:err});
    }
}

const getSales = async(req,res) => {
    try {
        let sales = await Sale.find();
        res.send(sales);
    }catch(err){
        res.send({error:err});
    }
}

const getSalesByStore = async(req,res) => {
    try {
        let sales = await Sale.find({storeId: req.query.storeId});
        res.send(sales);
    }catch(err){
        res.send({error:err});
    }
}

const createSale = async(req,res) => {
    try {
        let store = await Store.findOne({id: req.body.storeId});
        let client = await Client.findOne({id: req.body.clientId});

        let sale = await Sale.create({
            storeId: store.id,
            storeName: store.name,
            clientId: client.id,
            clientName: client.name,
            totalSale: req.body.totalSale,
            clientPayment: req.body.clientPayment
        });
        if(sale.totalSale > sale.clientPayment){
            await SaleDebt.create({
                saleId: sale.id,
                storeId: store.id,
                storeName: store.name,
                clientId: client.id,
                clientName: client.name,
                debt: (sale.totalSale - sale.clientPayment)
            });
            await updateIncrementAccountCurrentInternal(store.id, sale.clientPayment);
        }else {
            await updateIncrementAccountCurrentInternal(store.id, sale.totalSale);
        }
        
        res.send(sale);
    }catch(err){
        res.send({error:err});
    }
}

const updateSaleDebt = async(req,res)=> {
    try {
        
        let sale = await Sale.findOne({id: req.body.id});
        let saleDebt = await SaleDebt.findOne({saleId: sale.id});

        let payment = (req.body.debtPay + sale.clientPayment);

        if(payment == sale.totalSale){
            await SaleDebt.deleteOne({saleId: sale.id});
        }else{
            await SaleDebt.updateOne({id: saleDebt.id}, { $set: {debt: (saleDebt.debt - req.body.debtPay) }})
        }

        await Sale.updateOne({id: sale.id}, { $set: { clientPayment: payment } });
        await updateIncrementAccountCurrentInternal(sale.storeId, req.body.debtPay);

        let sales = await Sale.find();

        res.send(sales);
    } catch(err){
        console.log(err)
        res.send({error:err});
    }
}

const cancelTotalSale = async(req,res) => {
    try {
        
        await Sale.updateOne({id: req.query.id}, { $set: { cancelation: true } });
        let sale = await Sale.findOne({id: req.query.id});
        
        await SaleDebt.deleteOne({saleId: sale.id});
        await updateDecrementAccountCurrentInternal(sale.storeId, sale.clientPayment);

        let productSales = await ProductSale.find({saleId: sale.id});
        if(productSales.length > 0){

            await Promise.all(
                productSales.map(async(product)=> {

                    await updateIncrementExistenceInventoryInternal(sale.storeId, product.quantity, product.productId);
                })
            )
        }
        let sales = await Sale.find();

        res.send(sales);
    }catch(err){
        res.send({error:err});
    }
}

// BETA
const cancelPartialSale = async(req,res) => {
    try {
        let sale = await Sale.findOne({id: req.body.id});

        let productsToRemove = req.body.productsIds;
        let productsQuantityToRemove = req.body.productsQuantity;
        let newPayment = req.body.clientPayment;
        let currentToRest = 0;

        let productSales = await ProductSale.find({saleId: sale.id});

        if(productSales.length > 0){
            await Promise.all(
                productSales.map(async(product)=> {
                    if(productsToRemove.indexOf(product.id) > -1){

                        let newQuantity = productsQuantityToRemove[productsToRemove.indexOf(product.id)];
                        currentToRest = currentToRest + (product.salePrice * newQuantity);

                        if(productsQuantityToRemove[productsToRemove.indexOf(product.id)] == product.quantity){

                            await Product.deleteOne({id: product.id});
                        }else{
                            await Product.updateOne({id: product.id}, { $set: {quantity: newQuantity} })
                        }
                    }
                })
            );

            await Sale.updateOne({id: sale.id}, {$set:{totalSale: sale.totalSale - currentToRest, clientPayment: newPayment}});
            if(newPayment > sale.totalSale - currentToRest){
                let debt = await SaleDebt.findOne({saleId: sale.id});
                if(debt){
                    SaleDebt.updateOne({id: debt.id}, {$set:{debt: (sale.totalSale - currentToRest) - newPayment}});
                }else{
                    await SaleDebt.create({
                        saleId: sale.id,
                        storeId: store.id,
                        storeName: store.name,
                        clientId: client.id,
                        clientName: client.name,
                        debt: (sale.totalSale - sale.clientPayment)
                    });
                }
            }
        }
        if(sale.clientPayment > newPayment) {
            await updateDecrementAccountCurrentInternal(sale.storeId, (newPayment - sale.clientPayment));
        } else if(sale.clientPayment < newPayment) {
            await updateIncrementAccountCurrentInternal(sale.storeId, (newPayment - sale.clientPayment));
        }

        let sales = await Sale.find();
        res.send(sales);

    }catch(err){
        res.send({error: err});
    }
}

const deleteSalesInternal = async(storeId)=> {
    try {
        let sales = await Sale.find({storeId: storeId});
        if(sales.length > 0) {

            await Promise.all(
                sales.map(async(sale)=> {
                    await ProductSale.deleteMany({saleId: sale.id});
                    await SaleDebt.deleteMany({saleId: sale.id});
                    await Sale.deleteOne({id: sale.id});
                })
            );

        }
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const getProductSalesBySaleId = async(req,res) => {
    try {
        let productSales = await ProductSale.find({saleId: req.query.saleId});
        res.send(productSales);
    }catch(err){
        res.send({error:err});
    }
}

const createProductSale = async(req,res) => {
    try {
        let productSale = await ProductSale.create({
            saleId: req.body.saleId,
            productId: req.body.productId,
            name: req.body.name,
            quantity: req.body.quantity,
            salePrice: req.body.salePrice
        });

        let sale = await Sale.findOne({id: productSale.saleId});
        await updateDecrementExistenceInventoryInternal(sale.storeId, productSale.quantity, productSale.productId);
        res.send({msg: 'ok', status: 200, error: null});
    }catch(err){
        res.send({error:err});
    }
}

const getSaleDebt = async(req,res) => {
    try {
        let debt = await SaleDebt.findOne({saleId: req.query.saleId});
        res.send(debt);
    }catch(err){
        res.send({error:err});
    }
}
module.exports = {getExistenceProducts, getSales, getSalesByStore,createSale, updateSaleDebt, cancelTotalSale, cancelPartialSale, deleteSalesInternal, getProductSalesBySaleId, createProductSale, getSaleDebt};