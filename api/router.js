const {getHome, getDetail} = require('./home/home');
const {getStores, createStore, updateStore, deleteStore} = require('./stores/stores');
const {getProducts, createProduct, updateProduct, deleteProduct} = require('./products/products');
const {getClients, createClient, updateClient, deleteClient} = require('./clients/clients');
const {getInventories, getExistence, createExistence, updateExistence, deleteExistence, transferExistence} = require('./inventories/inventories');
const {getExistenceProducts, getSales, getSalesByStore, createSale, updateSaleDebt, cancelTotalSale, cancelPartialSale, getProductSalesBySaleId, createProductSale, getSaleDebt} = require('./sales/sales');
const {getAccounts, addAccountCurrent} = require('./account/account');
const {getOutcomes, createOutcome, deleteOutcome, storeTraspass, withdrawCurrent, toCharge, getToPay, createToPay, updateToPay, deleteToPay} = require('./accounting/accounting');

module.exports = (APP) => {
    APP.get('/api/home', getHome);
    APP.get('/api/homedetail', getDetail);

    APP.get('/api/stores', getStores);
    APP.post('/api/stores',createStore);
    APP.put('/api/stores', updateStore);
    APP.delete('/api/stores', deleteStore);

    APP.get('/api/products', getProducts);
    APP.post('/api/products',createProduct);
    APP.put('/api/products', updateProduct);
    APP.delete('/api/products', deleteProduct);

    APP.get('/api/clients', getClients);
    APP.post('/api/clients',createClient);
    APP.put('/api/clients', updateClient);
    APP.delete('/api/clients', deleteClient);

    APP.get('/api/inventories', getInventories);

    APP.get('/api/existences', getExistence);
    APP.post('/api/existences', createExistence);
    APP.put('/api/existences', updateExistence);
    APP.delete('/api/existences', deleteExistence);
    APP.post('/api/existences/transfer', transferExistence);

    APP.get('/api/sales', getSales);
    APP.post('/api/sales', createSale);
    APP.put('/api/sales', updateSaleDebt);
    APP.delete('/api/sales', cancelTotalSale);
    APP.get('/api/sales/store', getSalesByStore);
    APP.get('/api/sales/existence', getExistenceProducts);
    APP.get('/api/sales/productsales', getProductSalesBySaleId);
    APP.post('/api/sales/productsales', createProductSale);
    APP.get('/api/sales/debt', getSaleDebt);

    APP.get('/api/accounts', getAccounts);
    APP.post('/api/accounts/addCurrenct', addAccountCurrent);

    APP.get('/api/accounts/outcomes',getOutcomes);
    APP.post('/api/accounts/outcomes',createOutcome);
    APP.delete('/api/accounts/outcomes',deleteOutcome);
    APP.post('/api/accounts/withdraw',withdrawCurrent);
    APP.post('/api/accounts/traspass',storeTraspass);
    APP.get('/api/accounts/tocharge',toCharge);
    APP.get('/api/accounts/topay',getToPay);
    APP.post('/api/accounts/topay',createToPay);
    APP.put('/api/accounts/topay',updateToPay);
    APP.delete('/api/accounts/topay',deleteToPay);
};