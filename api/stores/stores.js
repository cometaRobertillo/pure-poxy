const Store = require('./stores.model');
const Inventory = require('../inventories/inventories.model');
const {createInventoryInternal, updateInventoryInternal, deleteInventoryInternal} = require('../inventories/inventories');
const {createAccountInternal, updateAccountStoreInternal, deleteAccountInternal} = require('../account/account');
const {deleteSalesInternal} = require('../sales/sales');
const {updateProductInternal, deleteProductInternal} = require('../products/products');

const getStores = async(req,res) => {
    try {
        let stores = await Store.find();
        res.send(stores);
    }catch(err){
        res.send({error:err});
    }
}

const createStore = async(req,res) => {
    try {

        let store = await Store.create({
            name: req.body.name
        });
        
        await createInventoryInternal(store.id, store.name);
        await createAccountInternal(store.id, store.name);

        let stores = await Store.find();
        res.send(stores);
    }catch(err){
        res.send({error:err});
    }
}

const updateStore = async(req,res) => {
    try {
        
        await Store.updateOne({id: req.body.id}, { $set: { name: req.body.name } });

        let storeInfo = {storeId: req.body.id, storeName: req.body.name};

        await updateInventoryInternal(storeInfo);
        await updateAccountStoreInternal(storeInfo);
        await updateProductInternal(storeInfo);

        let stores = await Store.find();
        res.send(stores);
    }catch(err){
        res.send({error:err});
    }
}

const deleteStore = async(req,res) => {
    try {
        await Store.deleteOne({id: req.query.id});

        let inventory = await Inventory.findOne({storeId: req.query.id});
        
        await deleteInventoryInternal(inventory);
        await deleteAccountInternal(req.query.id);
        await deleteSalesInternal(req.query.id);
        await deleteProductInternal(req.query.id);
        
        let stores = await Store.find();
        res.send(stores);
    }catch(err){
        res.send({error:err});
    }
}

module.exports = {getStores, createStore, updateStore, deleteStore};