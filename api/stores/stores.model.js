const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    name: { 
        type: mongoose.SchemaTypes.String,
        required: true
    },
};


const collectionName = "store";
const storeSchema = mongoose.Schema(schema);

storeSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'store', field: 'id', startAt: 1, incrementBy: 1});

const Store = mongoose.model(collectionName, storeSchema);
module.exports = Store;