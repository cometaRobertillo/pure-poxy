const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');


const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    storeName: {
        type: mongoose.SchemaTypes.String,
        required: false
    }
};

const collectionName = "inventory";
const inventorySchema = mongoose.Schema(schema);

inventorySchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'inventory', field: 'id', startAt: 1, incrementBy: 1});

const Inventory = mongoose.model(collectionName, inventorySchema);
module.exports = Inventory;