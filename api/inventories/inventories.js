const Inventory = require('./inventories.model');
const Existence = require('./existences.model');
const Product = require('../products/products.model');

const getInventories = async(req,res) => {
    try {
        let inventories = await Inventory.find();
        res.send(inventories);
    }catch(err){
        res.send({error:err});
    }
}

const getExistence = async(req,res) => {
    try {
        let existence = await Existence.find({inventoryId: req.query.inventoryId});
        res.send(existence);
    }catch(err){
        res.send({error:err});
    }
}

const createInventoryInternal = async(storeId,storeName) => {
    try {
        await Inventory.create({
            storeId: storeId,
            storeName: storeName
        });
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const createExistence = async(req,res) => {
    try {

        let product = await Product.findOne({id: req.body.productId});
        await Existence.create({
            inventoryId: req.body.inventoryId,
            productId: product.id,
            productName: product.name,
            quantity: req.body.quantity
        });

        let existences = await Existence.find({inventoryId: req.body.inventoryId});
        res.send(existences);
    }catch(err){
        res.send({error:err});
    }
}

const updateInventoryInternal = async(update) => {
    try {

        await Inventory.updateOne({storeId: update.storeId}, {
            $set: {
                    storeName: update.storeName,
                }
            }
        );
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const updateDecrementExistenceInventoryInternal = async(storeId, productQuantity, productId) => {
    try {

        let inventory = await Inventory.findOne({storeId: storeId});
        let existence = await Existence.findOne({inventoryId: inventory.id, productId: productId});
        let quantity = (existence.quantity - productQuantity);
        await Existence.updateOne({id: existence.id}, { $set: {quantity: quantity}});
        
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const updateIncrementExistenceInventoryInternal = async(storeId, productQuantity, productId) => {
    try {
        let inventory = await Inventory.findOne({storeId: storeId});
        let existence = await Existence.findOne({inventoryId: inventory.id, productId: productId});
        let quantity = (existence.quantity + productQuantity);
        await Existence.updateOne({id: existence.id}, { $set: {quantity: quantity}});
        return {msg: 'ok', error: null};
    }catch(err){
        console.log(err)
        return {msg: 'error', error: err};
    }
}

const updateExistence = async(req,res) => {
    try {

        await Existence.updateOne({id: req.body.id}, {
            $set: {
                    quantity: req.body.quantity,
                }
            }
        );

        let existences = await Existence.find({inventoryId: req.body.inventoryId});
        res.send(existences);
    }catch(err){
        res.send({error:err});
    }
}

const updateExistenceInternal = async(existenceId, toRest) => {
    try {
        let existence = await Existence.findOne({id: existenceId});
        let newValue = (existence.quantity - toRest);
        await Existence.updateOne({id: existenceId}, {
            $set: {
                    quantity: newValue,
                }
            }
        );

        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const deleteInventoryInternal = async(inventory) => {
    try {
        
        await deleteExistenceInternal(inventory.id);
        await Inventory.deleteOne({id: inventory.id});
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const deleteExistence = async(req,res) => {
    try {
        await Existence.deleteOne({id: req.query.id});
        let existences = await Existence.find({inventoryId: req.query.inventoryId});
        res.send(existences);
    }catch(err){
        res.send({error:err});
    }
}

const deleteExistenceInternal = async(inventoryId) => {
    try {
        await Existence.deleteMany({inventoryId: inventoryId});
        return {msg: 'ok', error: null};
    }catch(err){
        return {msg: 'error', error: err};
    }
}

const transferExistence = async(req,res) => {
    try {
        // {fromInventoryId: number, productId: number, existenceId:number, quantity:number, toInventoryId:number}
        console.log(req.body)
        let product = await Product.findOne({id: req.body.productId});
        
        let existence = await Existence.findOne({inventoryId: req.body.toInventoryId, productId: req.body.productId});
        if(existence) {
            await Existence.updateOne({id: existence.id}, {
                $set: {
                        quantity: existence.quantity + req.body.quantity,
                    }
                }
            );
        }else {
            await Existence.create({
                inventoryId: req.body.toInventoryId,
                productId: product.id,
                productName: product.name,
                quantity: req.body.quantity
            });
        }
        let currentExistence = await Existence.findOne({id: req.body.existenceId});
        await Existence.updateOne({id: currentExistence.id}, {
            $set: {
                    quantity: (currentExistence.quantity - req.body.quantity),
                }
            }
        );

        let existences = await Existence.find({inventoryId: req.body.fromInventoryId});
        res.send(existences);
    }catch(err){
        res.send({error:err});
    }
}

module.exports = {getInventories, getExistence, createInventoryInternal, createExistence, updateInventoryInternal, updateExistence, updateExistenceInternal, deleteInventoryInternal, deleteExistence, deleteExistenceInternal, updateDecrementExistenceInventoryInternal, updateIncrementExistenceInventoryInternal, transferExistence};