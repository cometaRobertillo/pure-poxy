const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    inventoryId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    productId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    productName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    quantity: {
        type: mongoose.SchemaTypes.Number,
        required: true
    }
};

const collectionName = "existence";
const existenceSchema = mongoose.Schema(schema);

existenceSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'existence', field: 'id', startAt: 1, incrementBy: 1});


const Existence = mongoose.model(collectionName, existenceSchema);
module.exports = Existence;