const Client = require('./clients.model');

const init = async() => {
    try {
        let client = await Client.findOne({name:'Público general'});
        if(!client){
            await Client.create({
                name: 'Público general',
                contactName: 'Sin contacto',
                contact: 'Sin contacto'
            });
        }
    }catch(err){
        console.log(err)
    }
}
init();
const getClients = async(req,res) => {
    try {
        let clients = await Client.find();
        res.send(clients);
    }catch(err){
        res.send({error:err});
    }
}

const createClient = async(req,res) => {
    try {
        await Client.create({
            name: req.body.name,
            contactName: req.body.contactName,
            contact: req.body.contact
        });
        let clients = await Client.find();
        res.send(clients);
    }catch(err){
        res.send({error:err});
    }
}

const updateClient = async(req,res) => {
    try {

        await Client.updateOne({id: req.body.id}, {
            $set: {
                    name: req.body.name,
                    contactName: req.body.contactName,
                    contact: req.body.contact
                }
            }
        );

        let clients = await Client.find();
        res.send(clients);
    }catch(err){
        res.send({error:err});
    }
}

const deleteClient = async(req,res) => {
    try {
        await Client.deleteOne({id: req.query.id});
        let clients = await Client.find();
        res.send(clients);
    }catch(err){
        res.send({error:err});
    }
}

module.exports = {getClients, createClient, updateClient, deleteClient};