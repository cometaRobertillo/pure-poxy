const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    name: { 
        type: mongoose.SchemaTypes.String,
        required: true
    },
    contactName: {
        type: mongoose.SchemaTypes.String,
        required: false
    },
    contact: {
        type: mongoose.SchemaTypes.String,
        required: false
    }
};

const collectionName = "client";
const clientSchema = mongoose.Schema(schema);

clientSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'client', field: 'id', startAt: 1, incrementBy: 1});

const Client = mongoose.model(collectionName, clientSchema);
module.exports = Client;