const Outcome = require('./outcome.model');
const Account = require('../account/accounts.model');
const Client = require('../clients/clients.model');
const SaleDebt = require('../sales/saleDebt.model');
const ToPay = require('./topay.model');
const {updateIncrementAccountCurrentInternal, updateDecrementAccountCurrentInternal} = require('../account/account');

const getOutcomes = async(req,res) => {
    try{
        let outcomes = await Outcome.find({accountId: req.query.accountId});
        res.send(outcomes);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const createOutcome = async(req,res) => {
    try{

        await Outcome.create({
            accountId: req.body.accountId,
            typeId: req.body.typeId,
            typeName: req.body.typeName,
            description: req.body.description,
            cost: req.body.cost
        });

        let fromAccount = await Account.findOne({id: req.body.accountId});

        await updateDecrementAccountCurrentInternal(fromAccount.storeId, req.body.cost);

        let outcomes = await Outcome.find({accountId: req.body.accountId});

        res.send(outcomes);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const deleteOutcome = async(req,res) => {
    try{

        let outcome = await Outcome.findOne({id: req.query.id});
        let toAccount = await Account.findOne({id: req.query.accountId});

        await updateIncrementAccountCurrentInternal(toAccount.storeId, outcome.cost);

        await Outcome.deleteOne({id: outcome.id});

        let outcomes = await Outcome.find({accountId: toAccount.id});
        res.send(outcomes);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const withdrawCurrent = async(req,res) => {
    try {

        let fromAccount = await Account.findOne({id: req.body.accountId});
        await updateDecrementAccountCurrentInternal(fromAccount.storeId, req.body.current);

        res.send({msg: 'ok', status: 200});
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
    
}

const storeTraspass = async(req,res) => {
    try {
        
        let fromAccount = await Account.findOne({id: req.body.fromAccountId});
        let toAccount = await Account.findOne({id: req.body.toAccountId});

        await updateDecrementAccountCurrentInternal(fromAccount.storeId, req.body.current);
        await updateIncrementAccountCurrentInternal(toAccount.storeId, req.body.current);

        res.send({msg: 'ok', status: 200});
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const toCharge = async(req,res) => {
    try {
        let debts = await SaleDebt.find({storeId: req.query.storeId});
        res.send(debts);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const getToPay = async(req,res) => {
    try {
        let topays = await ToPay.find({accountId: req.query.accountId});

        res.send(topays);
    }catch(err){
        res.send({error: err});
    }
}

const createToPay = async(req,res) => {
    try {

        await ToPay.create({
            accountId: req.body.accountId,
            description: req.body.description,
            debt: req.body.debt
        })
        let topays = await ToPay.find({accountId: req.query.accountId});
        res.send(topays);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const updateToPay = async(req,res) => {
    try {

        let topay = await ToPay.findOne({accountId: req.body.accountId});
        let fromAccount = await Account.findOne({id: topay.accountId});

        await updateDecrementAccountCurrentInternal(fromAccount.storeId, req.body.debt);
        await ToPay.deleteOne({id: topay.id});
        let topays = await ToPay.find({accountId: req.query.accountId});
        res.send(topays);
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

const deleteToPay = async(req,res) => {
    try {

        await ToPay.deleteOne({id: req.query.topayId});

        res.send({msg: 'ok', status: 200});
    }catch(err){
        console.log(err)
        res.send({error: err});
    }
}

module.exports = {getOutcomes, createOutcome, deleteOutcome, storeTraspass, withdrawCurrent, toCharge, getToPay, createToPay, updateToPay, deleteToPay};