const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    accountId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    typeId: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    typeName: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    description: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    cost: {
        type: mongoose.SchemaTypes.Number,
        required: true
    }
};

const collectionName = "outcome";
const outcomeSchema = mongoose.Schema(schema);

outcomeSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'outcome', field: 'id', startAt: 1, incrementBy: 1});

const Outcome = mongoose.model(collectionName, outcomeSchema);
module.exports = Outcome;