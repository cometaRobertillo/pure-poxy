const mongoose = require("../db");
const {MongooseAutoIncrementID} = require('mongoose-auto-increment-reworked');

const schema = {
    id: {
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    accountId: { 
        type: mongoose.SchemaTypes.Number,
        required: true
    },
    description: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    debt: {
        type: mongoose.SchemaTypes.Number,
        required: true
    }
};

const collectionName = "topay";
const topaySchema = mongoose.Schema(schema);

topaySchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'topay', field: 'id', startAt: 1, incrementBy: 1});

const ToPay = mongoose.model(collectionName, topaySchema);
module.exports = ToPay;