const Account = require('../account/accounts.model');
const Store = require('../stores/stores.model');
const SaleDebt = require('../sales/saleDebt.model');
const ToPay = require('../accounting/topay.model');

const getHome = async(req,res) => {
    try {
        let stores = await Store.find();
        await Promise.all(
            stores.map(async(store)=>{
                let account = await Account.findOne({storeId: store.id});
                let debts = await SaleDebt.countDocuments({storeId: store.id});
                let toPays = await ToPay.countDocuments({accountId: account.id});

                store.toPays = toPays;
                store.debts = debts;
                store.account = account;
            })
        )
        res.send(stores);
    }catch (err){
        console.log(err);
        res.send({error: err});
    }

}

const getDetail = async(req,res) => {
    try {

        let result = {};

        let account = await Account.findOne({storeId: req.query.storeId});
        let debts = await SaleDebt.countDocuments({storeId: req.query.storeId});
        let toPays = await ToPay.countDocuments({accountId: account.id});

        result.toPays = toPays;
        result.debts = debts;
        result.account = account;
        
        res.send(result);
    }catch (err){
        console.log(err);
        res.send({error: err});
    }

}
module.exports = {getHome, getDetail};