'use strict';

const
    COMPRESSION = require('compression'),
    EXPRESS = require('express'),
    APP = EXPRESS(),
    CORS = require('cors'),
    BODYPARSER = require('body-parser'),
    ROUTER = require(`${__dirname}/api/router`),
    PORT = 3000;

let init = () => {
    console.log('starting process');
    APP.use(EXPRESS.static(`${__dirname}/pure-poxy-app/dist/pure-poxy`));
    APP.use(COMPRESSION());
    APP.use(BODYPARSER.json({limit: '100mb', extended: true}));
    APP.use(BODYPARSER.urlencoded({limit: '100mb', extended: true}));

    APP.use(CORS());

    APP.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    try{
        ROUTER(APP);
    }catch(err){
        console.log(err);
    }
    
    APP.listen(PORT);
    console.log('running on ', PORT)
}

module.exports = {init};