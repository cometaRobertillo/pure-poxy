const {app, BrowserWindow, screen} = require('electron');
const path = require('path');
const {init} = require(`${__dirname}/server.js`);

let startWindow;
init();

const startApp = () => {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize;
  startWindow = new BrowserWindow({
    width: width,
    height: height,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    },
    frame: true,
    resizable: false
  });
  startWindow.setMenuBarVisibility(false);
  startWindow.removeMenu();
  // startWindow.loadFile(`${__dirname}/pure-poxy/dist/pure-poxy/index.html`);
  startWindow.loadURL('http://localhost:3000');
  startWindow.webContents.on("devtools-opened", () => { startWindow.webContents.closeDevTools(); });
  // startWindow.webContents.openDevTools()

  startWindow.on('closed', () => {
    startWindow = null
  });
};

app.on('ready', startApp);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
});

app.on('activate', () => {
  if (startWindow === null) startApp()
});